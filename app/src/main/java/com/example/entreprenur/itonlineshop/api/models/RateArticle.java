package com.example.entreprenur.itonlineshop.api.models;

import java.util.Date;

public class RateArticle {

    private String KupacId;
    private Integer OcjenaVrijednost;
    private Date Datum;
    private String ArtikalId;
    private String Komentar;

    public void setKomentar(String komentar) {
        Komentar = komentar;
    }

    public void setKupacId(String kupacId) {
        KupacId = kupacId;
    }

    public void setOcjenaVrijednost(Integer ocjenaVrijednost) {
        OcjenaVrijednost = ocjenaVrijednost;
    }

    public void setDatum(Date datum) {
        Datum = datum;
    }

    public void setArtikalId(String artikalId) {
        ArtikalId = artikalId;
    }
}
