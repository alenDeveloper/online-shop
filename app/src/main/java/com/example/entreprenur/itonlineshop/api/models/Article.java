package com.example.entreprenur.itonlineshop.api.models;

public class Article {

    private String Cijena;
    private String Kolicina;
    private String Sifra;
    private String Naziv;
    private String SlikaThumb;
    private String Id;

    public String getCijena() {
        return Cijena;
    }

    public String getKolicina() {
        return Kolicina;
    }

    public String getSifra() {
        return Sifra;
    }

    public String getNaziv() {
        return Naziv;
    }

    public String getSlikaThumb() {
        return SlikaThumb;
    }

    public String getId() {
        return Id;
    }

    public void setCijena(String cijena) {
        Cijena = cijena;
    }

    public void setKolicina(String kolicina) {
        Kolicina = kolicina;
    }

    public void setSifra(String sifra) {
        Sifra = sifra;
    }

    public void setNaziv(String naziv) {
        Naziv = naziv;
    }

    public void setSlikaThumb(String slikaThumb) {
        SlikaThumb = slikaThumb;
    }

    public void setId(String id) {
        Id = id;
    }

    @Override
    public String toString() {
        return "Article{" +
                "Cijena=" + Cijena +
                ", Kolicina='" + Kolicina + '\'' +
                ", Sifra='" + Sifra + '\'' +
                ", Naziv='" + Naziv + '\'' +
                ", SlikaThumb=" + SlikaThumb +
                ", Id=" + Id +
                '}';
    }
}
