package com.example.entreprenur.itonlineshop.api;

import com.example.entreprenur.itonlineshop.api.models.Article;
import com.example.entreprenur.itonlineshop.api.models.Category;
import com.example.entreprenur.itonlineshop.api.models.HistoryArticle;
import com.example.entreprenur.itonlineshop.api.models.OrderRequest;
import com.example.entreprenur.itonlineshop.api.models.RateArticle;
import com.example.entreprenur.itonlineshop.api.models.Review;
import com.example.entreprenur.itonlineshop.api.models.User;
import com.example.entreprenur.itonlineshop.api.models.UserRegister;
import com.example.entreprenur.itonlineshop.api.models.UserRegisterResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Client {

    @GET("Artikli/GetArtikliByKategorija")
    Call<List<Article>> getArticleByCategory(@Query("Id") int id);

    @GET("PodKategorija/GetPodKategorije")
    Call<List<Category>> getCategory();

    @GET("Artikli/GetArtikal")
    Call<Article> getArticle(@Query("ARTIKALID") int articleId);

    @POST("api/Kupci/Registracija")
    Call<UserRegisterResponse> register(@Body UserRegister user);

    @GET("api/Narudzba/SelectHistorijaNarudzbi/{userID}")
    Call<List<HistoryArticle>> getHistory(@Path("userID") String userID);

    @GET("api/Artikli/GetOcjeneArtikla/{id}")
    Call<List<Review>> getReviews(@Path("id") String id);

    @POST("api/Artikli/OcjeniArtikal")
    Call<Void> rateArticle(@Body RateArticle rateArticle);

    @GET("/api/Kupci/KupacLogin/{username}/{password}")
    Call<User> login(@Path("username") String username,
                           @Path("password") String password);

    @POST("api/Narudzba/DodajNarudzbu")
    Call<Void> sendOrder(@Body OrderRequest orderRequest);
}
