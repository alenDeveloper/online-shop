package com.example.entreprenur.itonlineshop.util;

import android.content.Context;
import android.content.SharedPreferences;

public class UserSharedPreferences {

    public static final String USER_SHARED_PREFERENCES = "UserSharedPreferences";
    public static final String USER_NAME = "UserName";
    public static final String USER_ID = "UserId";

    SharedPreferences userSharedPreferences = null;

    public UserSharedPreferences(Context context) {
        userSharedPreferences = context.getSharedPreferences(USER_SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }


    public void setUserName(String name) {
        if (userSharedPreferences != null) {
            userSharedPreferences.edit().putString(USER_NAME, name).apply();
        }
    }

    public String getUserName() {
        if (userSharedPreferences != null) {
            return userSharedPreferences.getString(USER_NAME, "");
        }
        return  "";
    }

    public void setUserId(String id) {
        if (userSharedPreferences != null) {
            userSharedPreferences.edit().putString(USER_ID, id).apply();
        }
    }

    public String getUserID() {
        if (userSharedPreferences != null) {
            return userSharedPreferences.getString(USER_ID, "");
        }
        return  "";
    }

}
