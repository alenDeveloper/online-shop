package com.example.entreprenur.itonlineshop.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserRegister {
    @SerializedName("Ime")
    @Expose
    public String Ime;

    @SerializedName("Prezime")
    @Expose
    public String Prezime;

    @SerializedName("Email")
    @Expose
    public String Email;

    @SerializedName("Lozinka")
    @Expose
    public String Lozinka;

    @SerializedName("KorisnickoIme")
    @Expose
    public String KorisnickoIme;

    @SerializedName("Status")
    @Expose
    public Integer Status;

    public void setIme(String ime) {
        Ime = ime;
    }

    public void setPrezime(String prezime) {
        Prezime = prezime;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setLozinka(String lozinka) {
        Lozinka = lozinka;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        KorisnickoIme = korisnickoIme;
    }

    public void setStatus(Integer status) {
        Status = status;
    }
}
