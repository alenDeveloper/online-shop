package com.example.entreprenur.itonlineshop.fragment.history;

import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.activity.MainActivity;
import com.example.entreprenur.itonlineshop.api.Client;
import com.example.entreprenur.itonlineshop.api.ServiceGenerator;
import com.example.entreprenur.itonlineshop.api.models.RateArticle;
import com.example.entreprenur.itonlineshop.util.UserSharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EFragment(R.layout.fragment_add_review)
public class AddReviewFragment extends Fragment {

    @FragmentArg
    public String articleId;

    @ViewById(R.id.rate)
    protected RatingBar ratingBar;
    @ViewById(R.id.comment_id)
    protected EditText customerRate;
    UserSharedPreferences userSharedPreferences;


    @AfterViews
    public void init() {
        userSharedPreferences = new UserSharedPreferences(getContext());
    }

    @Click(R.id.rate_btn)
    public void rate() {
        Integer rateValue = ratingBar.getNumStars();
        String rateComment = customerRate.getText().toString();
        RateArticle rateArticle = new RateArticle();
        rateArticle.setKomentar(rateComment);
        rateArticle.setArtikalId(articleId);
        rateArticle.setOcjenaVrijednost(rateValue);
        rateArticle.setDatum(new Date());
        rateArticle.setKupacId(userSharedPreferences.getUserID());

        Call<Void> call = ServiceGenerator.createService(Client.class).rateArticle(rateArticle);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                MainActivity activity = (MainActivity) getActivity();

                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }

                if (response.isSuccessful()) {
                    Toast.makeText(activity, "Uspjesno", Toast.LENGTH_SHORT).show();
                    activity.onBackPressed();
                } else {
                    Toast.makeText(activity, "Neuspjesno", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                MainActivity activity = (MainActivity) getActivity();

                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }
                Toast.makeText(activity, "Internet greska", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
