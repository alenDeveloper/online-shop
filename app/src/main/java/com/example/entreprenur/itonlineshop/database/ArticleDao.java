package com.example.entreprenur.itonlineshop.database;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ArticleDao {

    @Query("SELECT * FROM DatabaseArticleModel")
    List<DatabaseArticleModel> getAllArticles();


    @Insert
    void insertAll(DatabaseArticleModel article);

    @Query("DELETE FROM DatabaseArticleModel WHERE articleId = :articleId")
    int deleteSingle(String articleId);

    @Query("DELETE FROM DatabaseArticleModel")
    int deleteAll();

}
