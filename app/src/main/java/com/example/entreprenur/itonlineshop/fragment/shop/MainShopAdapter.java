package com.example.entreprenur.itonlineshop.fragment.shop;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.api.models.Category;

import java.nio.charset.Charset;
import java.util.List;

public class MainShopAdapter extends RecyclerView.Adapter<MainShopAdapter.ViewHolder> {

    private Context context;
    private List<Category> mainList;
    private OnShopElementCLick listener;

    public MainShopAdapter(Context context, List<Category> mainList, OnShopElementCLick listener) {
        this.context = context;
        this.mainList = mainList;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_list_item, parent, false);
        return new ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Category category = mainList.get(position);

        if (category == null) {
            return;
        }

        if (category.getNaziv() != null) {
            holder.itemName.setText(category.getNaziv());
        }
        if (category.getId() != null) {
            holder.itemId = category.getId();
        }
        if (category.getSlikaThumb() != null) {
            byte[] blob= Base64.decode(category.getSlikaThumb().getBytes(Charset.forName("UTF-8")),Base64.DEFAULT);
            Bitmap bmp = BitmapFactory.decodeByteArray(blob , 0, blob.length);
            holder.itemImage.setImageBitmap(bmp);
        }
    }

    @Override
    public int getItemCount() {
        return mainList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView itemImage;
        private TextView itemName;
        private Integer itemId;

        public ViewHolder(View itemView, final OnShopElementCLick listener) {
            super(itemView);

            itemImage = itemView.findViewById(R.id.item_image);
            itemName = itemView.findViewById(R.id.item_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onShopArticleClick(itemId);
                }
            });
        }
    }

    public interface OnShopElementCLick {
        void onShopArticleClick(int id);
    }
}
