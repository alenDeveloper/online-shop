package com.example.entreprenur.itonlineshop.api.models;


import java.util.Date;
import java.util.List;

public class OrderRequest {

    public String KupacId;
    public Date Datum;
    public Integer Status;
    public List<Order> NarudzbaStavke;

    public static class Order {
        public Order() {}
        public Integer Kolicina;
        public String ArtikalId;
    }
}
