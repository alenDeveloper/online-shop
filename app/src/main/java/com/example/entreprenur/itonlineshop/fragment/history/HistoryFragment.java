package com.example.entreprenur.itonlineshop.fragment.history;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.activity.MainActivity;
import com.example.entreprenur.itonlineshop.api.Client;
import com.example.entreprenur.itonlineshop.api.ServiceGenerator;
import com.example.entreprenur.itonlineshop.api.models.HistoryArticle;
import com.example.entreprenur.itonlineshop.util.UserSharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EFragment(R.layout.fragment_history)
public class HistoryFragment extends Fragment {

    @ViewById(R.id.main_list)
    protected RecyclerView mainList;
    @ViewById(R.id.swiperefresh)
    protected SwipeRefreshLayout swipeRefreshLayout;

    HistoryAdapter adapter;
    HistoryAdapter.OnArticleClick listener;
    UserSharedPreferences userSharedPreferences;

    @AfterViews
    public void init() {

        listener = new HistoryAdapter.OnArticleClick() {
            @Override
            public void onArticleClick(HistoryArticle article) {
                final MainActivity activity = (MainActivity) getActivity();

                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }
                activity.showAddReviewFragment(getFragmentManager(), String.valueOf(article.getIdArtikla()));
            }
        };

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        getData();
    }

    private void getData() {
        final MainActivity activity = (MainActivity) getActivity();
        userSharedPreferences = new UserSharedPreferences(activity);
        String userID = userSharedPreferences.getUserID();
        if (userID.isEmpty() || userID.equals("")) {
            Toast.makeText(activity, "Da vidite historiju morate biti logovani", Toast.LENGTH_SHORT).show();
            return;
        }
        Call<List<HistoryArticle>> call = ServiceGenerator.createService(Client.class).getHistory(userID);
        swipeRefreshLayout.setRefreshing(true);
        call.enqueue(new Callback<List<HistoryArticle>>() {
            @Override
            public void onResponse(Call<List<HistoryArticle>> call, Response<List<HistoryArticle>> response) {

                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }
                swipeRefreshLayout.setRefreshing(false);
                if (response == null || !response.isSuccessful()) {
                    return;
                }

                List<HistoryArticle> list = response.body();

                adapter = new HistoryAdapter(activity, list, listener);
                mainList.setLayoutManager(new LinearLayoutManager(activity));
                mainList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<HistoryArticle>> call, Throwable t) {
                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(activity, "Internet greska", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
