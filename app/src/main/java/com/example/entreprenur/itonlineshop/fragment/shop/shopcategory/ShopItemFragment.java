package com.example.entreprenur.itonlineshop.fragment.shop.shopcategory;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.activity.MainActivity;
import com.example.entreprenur.itonlineshop.api.Client;
import com.example.entreprenur.itonlineshop.api.ServiceGenerator;
import com.example.entreprenur.itonlineshop.api.models.Review;
import com.example.entreprenur.itonlineshop.database.DatabaseArticleModel;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.nio.charset.Charset;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EFragment(R.layout.shop_item_fragment)
public class ShopItemFragment extends Fragment {

    @FragmentArg
    public String name;
    @FragmentArg
    public String price;
    @FragmentArg
    public String image;
    @FragmentArg
    public String code;
    @FragmentArg
    public String id;

    @ViewById(R.id.amount_text)
    protected TextView amountText;
    @ViewById(R.id.shop_item_name)
    protected TextView itemName;
    @ViewById(R.id.shop_item_price)
    protected TextView itemPrice;
    @ViewById(R.id.item_image)
    protected ImageView itemImage;
    @ViewById(R.id.review_list)
    protected RecyclerView reviewList;

    int amount = 1;

    @AfterViews
    public void init() {
        itemName.setText(name);
        itemPrice.setText(price);

        byte[] blob= Base64.decode(image.getBytes(Charset.forName("UTF-8")),Base64.DEFAULT);
        Bitmap bmp = BitmapFactory.decodeByteArray(blob , 0, blob.length);
        itemImage.setImageBitmap(bmp);

        Call<List<Review>> call = ServiceGenerator.createService(Client.class).getReviews(id);
        call.enqueue(new Callback<List<Review>>() {
            @Override
            public void onResponse(Call<List<Review>> call, Response<List<Review>> response) {
                MainActivity activity = (MainActivity) getActivity();
                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }
                if (response == null) {
                    return;
                }

                List<Review> reviews = response.body();

                ReviewAdapter adapter = new ReviewAdapter(reviews);
                reviewList.setLayoutManager(new LinearLayoutManager(activity));
                reviewList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Review>> call, Throwable t) {
                MainActivity activity = (MainActivity) getActivity();
                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }
                Toast.makeText(activity, "Internet greska", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Click(R.id.btn_plus)
    public void increment() {
        amount++;

        amountText.setText(String.valueOf(amount));
    }

    @Click(R.id.btn_minus)
    public void decrement() {
        if (amount > 1) {
            amount--;
            amountText.setText(String.valueOf(amount));
        }
    }

    @Click(R.id.add_to_basket_btn)
    public void addToBasket() {
        MainActivity activity = (MainActivity) getActivity();
        if (activity == null || activity.isFinishing() || !isAdded()) {
            return;
        }

        DatabaseArticleModel articleModel = new DatabaseArticleModel();
        articleModel.setAmount(String.valueOf(amount));
        articleModel.setCode(code);
        articleModel.setArticleId(id);
        articleModel.setImage(image);
        articleModel.setName(name);
        articleModel.setPrice(price);

        activity.addArticleToDatabase(articleModel);
        activity.onBackPressed();
    }
}
