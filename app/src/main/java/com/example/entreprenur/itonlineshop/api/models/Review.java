package com.example.entreprenur.itonlineshop.api.models;

public class Review {

    private Integer OcjenaVrijednost;
    private String Komentar;
    private String ImeKupca;


    public Integer getOcjenaVrijednost() {
        return OcjenaVrijednost;
    }

    public String getKomentar() {
        return Komentar;
    }

    public String getImeKupca() {
        return ImeKupca;
    }
}
