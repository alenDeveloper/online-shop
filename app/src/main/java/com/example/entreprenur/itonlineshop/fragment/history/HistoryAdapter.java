package com.example.entreprenur.itonlineshop.fragment.history;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.api.models.HistoryArticle;

import java.nio.charset.Charset;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    Context context;
    List<HistoryArticle> mainList;
    HistoryAdapter.OnArticleClick listener;

    public HistoryAdapter(Context context, List<HistoryArticle> mainList, HistoryAdapter.OnArticleClick listener) {
        this.context = context;
        this.mainList = mainList;
        this.listener = listener;
    }

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_item, parent, false);
        return new HistoryAdapter.ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(HistoryAdapter.ViewHolder holder, int position) {
        HistoryArticle article = mainList.get(position);

        if (article == null) {
            return;
        }

        if (article.getNarucen() != null) {
            holder.articlePrice.setText(article.getDatumNarucivanja());
        }
        if (article.getNazivArtikla() != null) {
            holder.articleName.setText(article.getNazivArtikla());
        }
        if (article.getSlikaThumb() != null) {
            byte[] blob= Base64.decode(article.getSlikaThumb().getBytes(Charset.forName("UTF-8")),Base64.DEFAULT);
            Bitmap bmp = BitmapFactory.decodeByteArray(blob , 0, blob.length);
            holder.articleImage.setImageBitmap(bmp);
        }

        holder.article = article;
    }

    @Override
    public int getItemCount() {
        return mainList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView articleName, articlePrice;
        ImageView articleImage;
        HistoryArticle article;


        public ViewHolder(View itemView, final HistoryAdapter.OnArticleClick listener) {
            super(itemView);

            articleImage = itemView.findViewById(R.id.article_iamge);
            articleName = itemView.findViewById(R.id.article_name);
            articlePrice = itemView.findViewById(R.id.article_prise);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onArticleClick(article);
                    }
                }
            });
        }
    }

    public interface OnArticleClick {
        void onArticleClick(HistoryArticle article);
    }
}
