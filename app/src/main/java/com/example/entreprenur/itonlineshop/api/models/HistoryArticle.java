package com.example.entreprenur.itonlineshop.api.models;

import java.util.Date;

public class HistoryArticle {

    private Integer IdArtikla;
    private String NazivArtikla;
    private String Narucen;
    private String DatumNarucivanja;
    private String SlikaThumb;


    public Integer getIdArtikla() {
        return IdArtikla;
    }

    public String getNazivArtikla() {
        return NazivArtikla;
    }

    public String getNarucen() {
        return Narucen;
    }

    public String getDatumNarucivanja() {
        return DatumNarucivanja;
    }

    public String getSlikaThumb() {
        return SlikaThumb;
    }
}
