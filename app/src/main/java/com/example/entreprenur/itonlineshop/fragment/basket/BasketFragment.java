package com.example.entreprenur.itonlineshop.fragment.basket;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.activity.MainActivity;
import com.example.entreprenur.itonlineshop.api.Client;
import com.example.entreprenur.itonlineshop.api.ServiceGenerator;
import com.example.entreprenur.itonlineshop.api.models.Article;
import com.example.entreprenur.itonlineshop.api.models.OrderRequest;
import com.example.entreprenur.itonlineshop.fragment.shop.shopcategory.ArticleAdapter;
import com.example.entreprenur.itonlineshop.util.UserSharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EFragment(R.layout.fragment_basket)
public class BasketFragment extends Fragment {

    @ViewById(R.id.basket_list)
    protected RecyclerView basketList;
    @ViewById(R.id.pay_sum)
    protected TextView paySum;
    @ViewById(R.id.finish_shop)
    protected ImageView button;

    ArticleAdapter.OnArticleLongClick onArticleLongClick;
    ArticleAdapter adapter;
    List<Article> mainList = new ArrayList<>();
    UserSharedPreferences userSharedPreferences;

    @AfterViews
    public void init() {
        final MainActivity activity = (MainActivity) getActivity();
        userSharedPreferences = new UserSharedPreferences(activity);

        onArticleLongClick = new ArticleAdapter.OnArticleLongClick() {
            @Override
            public void onArticleLongClick(Article article) {
                activity.deleteSpecific(article.getId());
                mainList = activity.getAllArticles();
                adapter.setList(mainList);
                setPaySum(mainList);
            }
        };

        mainList = activity.getAllArticles();
        if (mainList == null || mainList.isEmpty()) {
            return;
        }

        setPaySum(mainList);

        adapter = new ArticleAdapter(true, activity, mainList, null, onArticleLongClick);
        basketList.setLayoutManager(new LinearLayoutManager(activity));
        basketList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void setPaySum(List<Article> list) {
        int moneySum = 0;
        for (Article article : list) {
            if (article.getCijena() != null && !article.getCijena().isEmpty()) {
                String[] stirngList = article.getCijena().split("\\.");
                int articlePrice = Integer.parseInt(stirngList[0]);
                int articleCount = Integer.parseInt(article.getKolicina());
                int articleCost = articleCount * articlePrice;
                moneySum += articleCost;
            }
        }

        paySum.setText(String.valueOf(moneySum));
    }

    @Click(R.id.finish_shop)
    public void finishShop() {

        button.setClickable(false);
        final MainActivity activity = (MainActivity) getActivity();
        if (activity == null || activity.isFinishing()) {
            return;
        }

        if (mainList == null || mainList.isEmpty()) {
            return;
        }
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.Datum = new Date();
        orderRequest.KupacId = userSharedPreferences.getUserID();
        List<OrderRequest.Order> orders = new ArrayList<>();
        for (Article article : mainList) {

            OrderRequest.Order order = new OrderRequest.Order();
            order.ArtikalId = article.getId();
            order.Kolicina = Integer.valueOf(article.getKolicina());

            orders.add(order);
        }
        orderRequest.NarudzbaStavke = orders;

        Call<Void> call = ServiceGenerator.createService(Client.class).sendOrder(orderRequest);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                button.setClickable(true);
                if (response == null || !response.isSuccessful()) {
                    Toast.makeText(activity, "Niste uspjesno kupili", Toast.LENGTH_SHORT).show();
                    return;
                }

                basketList.setVisibility(View.GONE);
                paySum.setVisibility(View.GONE);
                Toast.makeText(activity, "Uspjesno ste kupili", Toast.LENGTH_SHORT).show();
                activity.deleteAll();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                button.setClickable(true);
                Toast.makeText(activity, "Internet greska", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
