package com.example.entreprenur.itonlineshop.fragment;


import android.support.v4.app.Fragment;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.activity.LoginActivity;
import com.example.entreprenur.itonlineshop.api.Client;
import com.example.entreprenur.itonlineshop.api.ServiceGenerator;
import com.example.entreprenur.itonlineshop.api.models.UserRegister;
import com.example.entreprenur.itonlineshop.api.models.UserRegisterResponse;
import com.example.entreprenur.itonlineshop.util.UserSharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EFragment(R.layout.fragment_register)
public class RegisterFragment extends Fragment {

    @ViewById(R.id.input_name)
    protected EditText inputName;
    @ViewById(R.id.input_lastname)
    protected EditText inputLastName;
    @ViewById(R.id.input_email)
    protected EditText inputEmail;
    @ViewById(R.id.input_address)
    protected EditText inputAddress;
    @ViewById(R.id.input_username)
    protected EditText inputUsername;
    @ViewById(R.id.input_password)
    protected EditText inputPassword;
    @ViewById(R.id.btn_register)
    protected Button btn;

    UserSharedPreferences userSharedPreferences;

    @AfterViews
    public void init() {
        LoginActivity activity = (LoginActivity) getActivity();
        if(activity == null || activity.isFinishing() || !isAdded()) {
            return;
        }

        userSharedPreferences = new UserSharedPreferences(activity);
    }
    @Click(R.id.btn_cancel)
    public void backPress() {
        LoginActivity activity = (LoginActivity) getActivity();
        if(activity == null || activity.isFinishing() || !isAdded()) {
            return;
        }

        activity.onBackPressed();
    }

    @Click(R.id.btn_register)
    public void registerUser() {

        LoginActivity activity = (LoginActivity) getActivity();
        if(activity == null || activity.isFinishing() || !isAdded()) {
            return;
        }

        if (checkIsEverythingSet()) {
            register();
        } else {
            Toast.makeText(activity, "Morate ispuniti sva polja", Toast.LENGTH_SHORT).show();
        }

    }

    private void register() {
        btn.setClickable(false);
        String name = inputName.getText().toString();
        String lastname = inputLastName.getText().toString();
        String email = inputEmail.getText().toString();
        String pass = inputPassword.getText().toString();

        UserRegister userRegister = new UserRegister();
        userRegister.setEmail(email);
        userRegister.setIme(name);
        userRegister.setLozinka(pass);
        userRegister.setPrezime(lastname);
        userRegister.setKorisnickoIme(name);
        userRegister.setStatus(1);

        final LoginActivity activity = (LoginActivity) getActivity();

        Call<UserRegisterResponse> call = ServiceGenerator.createService(Client.class).register(userRegister);
        call.enqueue(new Callback<UserRegisterResponse>() {
            @Override
            public void onResponse(Call<UserRegisterResponse> call, Response<UserRegisterResponse> response) {
                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }
                btn.setClickable(false);
                if (response == null || !response.isSuccessful()) {
                    return;
                }
                loginWithUser(response.body());
                activity.navigateToMain();
            }

            @Override
            public void onFailure(Call<UserRegisterResponse> call, Throwable t) {
                Toast.makeText(activity, "Internet greska", Toast.LENGTH_SHORT).show();
                btn.setClickable(false);
            }
        });
    }

    private void loginWithUser(UserRegisterResponse response) {
        userSharedPreferences.setUserId(response.Id);
        userSharedPreferences.setUserName(response.Ime);
    }

    private boolean checkIsEverythingSet() {
        return !inputName.getText().toString().isEmpty()
                && !inputLastName.getText().toString().isEmpty()
                && !inputEmail.getText().toString().isEmpty()
                && !inputAddress.getText().toString().isEmpty()
                && !inputUsername.getText().toString().isEmpty()
                && !inputPassword.getText().toString().isEmpty();
    }
}
