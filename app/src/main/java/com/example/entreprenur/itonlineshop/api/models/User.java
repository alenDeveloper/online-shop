package com.example.entreprenur.itonlineshop.api.models;

public class User {
    private String Id;
    private String Ime;
    private String Prezime;
    private String Email;
    private String KorisnickoIme;

    public String getId() {
        return Id;
    }

    public String getIme() {
        return Ime;
    }

    public String getPrezime() {
        return Prezime;
    }

    public String getEmail() {
        return Email;
    }

    public String getKorisnickoIme() {
        return KorisnickoIme;
    }
}
