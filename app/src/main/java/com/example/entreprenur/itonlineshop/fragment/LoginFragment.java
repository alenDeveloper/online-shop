package com.example.entreprenur.itonlineshop.fragment;


import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.Toast;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.activity.LoginActivity;
import com.example.entreprenur.itonlineshop.api.Client;
import com.example.entreprenur.itonlineshop.api.ServiceGenerator;
import com.example.entreprenur.itonlineshop.api.models.User;
import com.example.entreprenur.itonlineshop.util.UserSharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EFragment(R.layout.fragment_login)
public class LoginFragment extends Fragment {

    @ViewById(R.id.input_email)
    protected EditText inputEmail;
    @ViewById(R.id.input_password)
    protected EditText inputPassword;

    UserSharedPreferences userSharedPreferences;

    @AfterViews
    public void init() {
        final LoginActivity activity = (LoginActivity) getActivity();
        userSharedPreferences = new UserSharedPreferences(activity);
    }

    @Click(resName = "login_button")
    public void login() {
        final LoginActivity activity = (LoginActivity) getActivity();
        //userSharedPreferences = new UserSharedPreferences(activity);

        if (isAllFill()) {
            String username = inputEmail.getText().toString();
            String password = inputPassword.getText().toString();

            Call<User> call = ServiceGenerator.createService(Client.class).login(username, password);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (activity == null || activity.isFinishing() || !isAdded()) {
                        return;
                    }

                    if (response == null || !response.isSuccessful()) {
                        return;
                    }
                    loginWithUser(response.body());
                    activity.navigateToMain();
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(activity, "Korisnicko ime ili lozinka su netacni", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(activity, "Korisnicko ime ili lozinka nedostaju", Toast.LENGTH_SHORT).show();
        }
    }

    private void loginWithUser(User user) {
        userSharedPreferences.setUserId(user.getId());
        userSharedPreferences.setUserName(user.getIme());
    }

    @Click(resName = "register_button")
    public void register() {
        LoginActivity activity = (LoginActivity) getActivity();

        if (activity == null || activity.isFinishing() || !isAdded()) {
            return;
        }

        activity.navigateToRegister(getFragmentManager());
    }

    @Click(resName = "free_enter_button")
    public void freeEnter() {
        LoginActivity activity = (LoginActivity) getActivity();

        if (activity == null || activity.isFinishing() || !isAdded()) {
            return;
        }

        userSharedPreferences.setUserId("");
        userSharedPreferences.setUserName("");
        activity.navigateToMain();
    }

    private boolean isAllFill() {
        return !inputEmail.getText().toString().isEmpty()
                && !inputPassword.getText().toString().isEmpty();
    }
}
