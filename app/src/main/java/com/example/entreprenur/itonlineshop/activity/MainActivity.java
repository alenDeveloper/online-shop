package com.example.entreprenur.itonlineshop.activity;

import android.arch.persistence.room.Room;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.api.models.Article;
import com.example.entreprenur.itonlineshop.database.AppDatabase;
import com.example.entreprenur.itonlineshop.database.DatabaseArticleModel;
import com.example.entreprenur.itonlineshop.fragment.basket.BasketFragment_;
import com.example.entreprenur.itonlineshop.fragment.history.AddReviewFragment_;
import com.example.entreprenur.itonlineshop.fragment.history.HistoryFragment_;
import com.example.entreprenur.itonlineshop.fragment.shop.ShopFragment_;
import com.example.entreprenur.itonlineshop.fragment.shop.shopcategory.ShopCategoryFragment_;
import com.example.entreprenur.itonlineshop.fragment.shop.shopcategory.ShopItemFragment_;
import com.example.entreprenur.itonlineshop.util.UserSharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById(resName = "navigation")
    protected BottomNavigationView bottomNavigationView;

    public AppDatabase db;
    public UserSharedPreferences userSharedPreferences;

    @Override
    protected void onResume() {
        super.onResume();

        bottomNavigationView.setSelectedItemId(R.id.menu_shop);
        showShop(getSupportFragmentManager());
    }

    private Integer categoryId = 0;

    @AfterViews
    public void init() {

        userSharedPreferences = new UserSharedPreferences(MainActivity.this);
        db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "production")
                .allowMainThreadQueries()
                .build();


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_shop:
                        showShop(getSupportFragmentManager());
                        break;
                    case R.id.menu_basket:
                        showBasket(getSupportFragmentManager());
                        break;
                    case R.id.menu_history:
                        showHistory(getSupportFragmentManager());
                        break;
                    case R.id.menu_logout:
                        logout();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

    }

    public void addArticleToDatabase(DatabaseArticleModel articleModel) {
        db.articleDao().insertAll(articleModel);
    }

    public void deleteSpecific(String articleId) {
        db.articleDao().deleteSingle(articleId);
    }

    public void deleteAll() {
        db.articleDao().deleteAll();
    }

    public List<Article> getAllArticles() {

        List<DatabaseArticleModel> databseList = db.articleDao().getAllArticles();
        List<Article> list = new ArrayList<>();
        for (DatabaseArticleModel articleModel : databseList) {
            Article article = new Article();
            article.setCijena(articleModel.getPrice());
            article.setId(articleModel.getArticleId());
            article.setSlikaThumb(articleModel.getImage());
            article.setNaziv(articleModel.getName());
            article.setSifra(articleModel.getCode());
            article.setKolicina(articleModel.getAmount());

            list.add(article);
        }

        return list;
    }

    public void showShop(FragmentManager fragmentManager) {
        if (fragmentManager != null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container, ShopFragment_.builder().build(), ShopFragment_.class.getSimpleName())
                    .commitAllowingStateLoss();
        }
    }

    public void showBasket(FragmentManager fragmentManager) {
        if (fragmentManager != null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container, BasketFragment_.builder().build(), BasketFragment_.class.getSimpleName())
                    .commitAllowingStateLoss();
        }
    }

    public void showHistory(FragmentManager fragmentManager) {
        if (fragmentManager != null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container, HistoryFragment_.builder().build(), HistoryFragment_.class.getSimpleName())
                    .commitAllowingStateLoss();
        }
    }

    public void logout() {
        userSharedPreferences.setUserName("");
        userSharedPreferences.setUserId("");
        LoginActivity_.IntentBuilder_ builder_ = LoginActivity_.intent(MainActivity.this);
        builder_.start();
        finish();
    }

    public void showShopCategory(FragmentManager fragmentManager, Integer categoryId) {
        if (fragmentManager != null) {
            this.categoryId = categoryId;
            Fragment shopCategoryFragment = ShopCategoryFragment_.builder().id(categoryId).build();

            fragmentManager.beginTransaction()
                    .replace(R.id.container, shopCategoryFragment, ShopCategoryFragment_.class.getSimpleName())
                    .commitAllowingStateLoss();
        }
    }

    public void showShopItem(FragmentManager fragmentManager, String name, String price, String id, String image, String code) {
        if (fragmentManager != null) {

            Fragment itemShopFragment = ShopItemFragment_.builder()
                    .code(code)
                    .id(id)
                    .image(image)
                    .name(name)
                    .price(price)
                    .build();

            fragmentManager.beginTransaction()
                    .replace(R.id.container, itemShopFragment, ShopItemFragment_.class.getSimpleName())
                    .commitAllowingStateLoss();
        }
    }

    public void showAddReviewFragment(FragmentManager fragmentManager, String id) {
        if (fragmentManager != null) {

            Fragment addReviewFragment = AddReviewFragment_.builder()
                    .articleId(id)
                    .build();

            fragmentManager.beginTransaction()
                    .replace(R.id.container, addReviewFragment, AddReviewFragment_.class.getSimpleName())
                    .commitAllowingStateLoss();
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null && fragmentManager.getFragments() != null && !fragmentManager.getFragments().isEmpty()) {
            if (fragmentManager.getFragments().get(0).getTag().equals("ShopItemFragment_")) {
                showShopCategory(fragmentManager, categoryId);
            } else if (fragmentManager.getFragments().get(0).getTag().equals("ShopCategoryFragment_")) {
                showShop(fragmentManager);
            } else if (fragmentManager.getFragments().get(0).getTag().equals("AddReviewFragment_")) {
                showHistory(fragmentManager);
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }
}
