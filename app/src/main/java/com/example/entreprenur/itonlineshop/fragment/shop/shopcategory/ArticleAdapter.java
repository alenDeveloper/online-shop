package com.example.entreprenur.itonlineshop.fragment.shop.shopcategory;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.api.models.Article;

import java.nio.charset.Charset;
import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ViewHolder> {

    Context context;
    List<Article> mainList;
    OnArticleClick listener;
    OnArticleLongClick longClickListener;
    boolean fromBasket;

    public ArticleAdapter(boolean fromBasket, Context context, List<Article> mainList, OnArticleClick listener, OnArticleLongClick onArticleLongClick) {
        this.context = context;
        this.mainList = mainList;
        this.listener = listener;
        this.longClickListener = onArticleLongClick;
        this.fromBasket = fromBasket;
    }

    public void setList(List<Article> mainList) {
        this.mainList = mainList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_item, parent, false);
        return new ArticleAdapter.ViewHolder(view, listener, longClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Article article = mainList.get(position);

        if (article == null) {
            return;
        }

        if (fromBasket) {
            if (article.getKolicina() != null) {
                holder.articleCount.setText(String.format("%s x", article.getKolicina()));
            }
        } else {
            holder.articleCount.setVisibility(View.GONE);
        }
        if (article.getCijena() != null) {
            holder.articlePrice.setText(article.getCijena());
        }
        if (article.getNaziv() != null) {
            holder.articleName.setText(article.getNaziv());
        }
        if (article.getSlikaThumb() != null) {
            byte[] blob= Base64.decode(article.getSlikaThumb().getBytes(Charset.forName("UTF-8")),Base64.DEFAULT);
            Bitmap bmp = BitmapFactory.decodeByteArray(blob , 0, blob.length);
            holder.articleImage.setImageBitmap(bmp);
        }

        holder.article = article;
    }

    @Override
    public int getItemCount() {
        return mainList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView articleName, articlePrice, articleCount;
        ImageView articleImage;
        Article article;


        public ViewHolder(View itemView, final OnArticleClick listener, final OnArticleLongClick longClick) {
            super(itemView);

            articleCount = itemView.findViewById(R.id.count_number);
            articleImage = itemView.findViewById(R.id.article_iamge);
            articleName = itemView.findViewById(R.id.article_name);
            articlePrice = itemView.findViewById(R.id.article_prise);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onArticleClick(article);
                    }
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (longClick != null) {
                        longClick.onArticleLongClick(article);
                    }
                    return false;
                }
            });
        }
    }

    public interface OnArticleClick {
        void onArticleClick(Article article);
    }

    public interface OnArticleLongClick {
        void onArticleLongClick(Article article);
    }
}
