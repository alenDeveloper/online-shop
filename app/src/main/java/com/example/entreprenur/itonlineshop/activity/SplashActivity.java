package com.example.entreprenur.itonlineshop.activity;

import android.animation.Animator;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.util.UserSharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity {

    @ViewById(R.id.splash_text)
    protected TextView splashText;
    UserSharedPreferences userSharedPreferences;

    @AfterViews
    public void init() {
        userSharedPreferences = new UserSharedPreferences(SplashActivity.this);
        doAnimation();
    }


    private void navigateToMain(boolean isLogged) {
        if (isLogged) {
            MainActivity_.IntentBuilder_ builder = MainActivity_.intent(SplashActivity.this);
            builder.start();
        } else {
            LoginActivity_.IntentBuilder_ builder = LoginActivity_.intent(SplashActivity.this);
            builder.start();
        }
        finish();
    }

    private void doAnimation() {
        splashText.animate().alphaBy(1).setDuration(1000).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (userSharedPreferences.getUserID().isEmpty()) {
                    navigateToMain(false);
                } else {
                    navigateToMain(true);
                }

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }
}
