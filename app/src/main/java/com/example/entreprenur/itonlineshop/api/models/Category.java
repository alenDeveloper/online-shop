package com.example.entreprenur.itonlineshop.api.models;

public class Category {

    private Integer Id;
    private String Naziv;
    private String SlikaThumb;

    @Override
    public String toString() {
        return "Category{" +
                "Id=" + Id +
                ", Naziv='" + Naziv + '\'' +
                ", SlikaThumb='" + SlikaThumb + '\'' +
                '}';
    }

    public Integer getId() {
        return Id;
    }

    public String getNaziv() {
        return Naziv;
    }

    public String getSlikaThumb() {
        return SlikaThumb;
    }
}
