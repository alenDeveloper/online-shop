package com.example.entreprenur.itonlineshop.fragment.shop.shopcategory;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.api.models.Review;
import com.example.entreprenur.itonlineshop.fragment.history.HistoryAdapter;

import java.util.ArrayList;
import java.util.List;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {

    List<Review> mainList = new ArrayList<>();

    public ReviewAdapter(List<Review> mainList) {
        this.mainList = mainList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_item, parent, false);
        return new ReviewAdapter.ViewHolder(view);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Review review = mainList.get(position);
        if (review == null) {
            return;
        }

        holder.rate.setText(String.format("Ocjena kupca: %d", review.getOcjenaVrijednost()));
        holder.name.setText(review.getImeKupca());
        holder.comment.setText(review.getKomentar());
    }

    @Override
    public int getItemCount() {
        return mainList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, comment, rate;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.customer_name);
            comment = itemView.findViewById(R.id.customer_comment);
            rate = itemView.findViewById(R.id.customer_rate);
        }
    }
}
