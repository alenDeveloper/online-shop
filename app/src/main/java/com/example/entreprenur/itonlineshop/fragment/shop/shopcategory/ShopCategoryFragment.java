package com.example.entreprenur.itonlineshop.fragment.shop.shopcategory;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.widget.Toast;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.activity.MainActivity;
import com.example.entreprenur.itonlineshop.api.Client;
import com.example.entreprenur.itonlineshop.api.ServiceGenerator;
import com.example.entreprenur.itonlineshop.api.models.Article;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EFragment(R.layout.fragment_shop_category)
public class ShopCategoryFragment extends Fragment {

    @ViewById(R.id.swiperefresh)
    protected SwipeRefreshLayout swipeRefreshLayout;
    @ViewById(R.id.shop_list)
    protected RecyclerView shopList;

    ArticleAdapter articleAdapter;
    ArticleAdapter.OnArticleClick listener;

    @FragmentArg
    public Integer id;

    @AfterViews
    public void init() {

        if (articleAdapter == null) {
            getData();
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        listener = new ArticleAdapter.OnArticleClick() {
            @Override
            public void onArticleClick(Article article) {
                MainActivity activity = (MainActivity) getActivity();
                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }

                activity.showShopItem(getFragmentManager(),
                        article.getNaziv(), article.getCijena(),
                        article.getId(), article.getSlikaThumb(),
                        article.getSifra());
            }
        };
    }

    private void getData() {

        final MainActivity activity = (MainActivity) getActivity();
        swipeRefreshLayout.setRefreshing(true);
        Call<List<Article>> call = ServiceGenerator.createService(Client.class).getArticleByCategory(id);
        call.enqueue(new Callback<List<Article>>() {
            @Override
            public void onResponse(Call<List<Article>> call, Response<List<Article>> response) {
                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }
                swipeRefreshLayout.setRefreshing(false);

                if (response == null) {
                    return;
                }

                List<Article> articleList = response.body();
                if (articleList == null || articleList.isEmpty()) {
                    return;
                }

                shopList.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
                articleAdapter = new ArticleAdapter(false, activity, articleList, listener, null);
                shopList.setAdapter(articleAdapter);

                articleAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Article>> call, Throwable t) {
                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }

                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(activity, "Internet greska", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
