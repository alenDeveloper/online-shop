package com.example.entreprenur.itonlineshop.fragment.shop;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.widget.Toast;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.activity.MainActivity;
import com.example.entreprenur.itonlineshop.api.Client;
import com.example.entreprenur.itonlineshop.api.ServiceGenerator;
import com.example.entreprenur.itonlineshop.api.models.Category;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EFragment(R.layout.fragment_shop)
public class ShopFragment extends Fragment {

    @ViewById(R.id.swiperefresh)
    protected SwipeRefreshLayout swipeRefreshLayout;
    @ViewById(R.id.shop_list)
    protected RecyclerView shopList;

    MainShopAdapter mainShopAdapter;
    MainShopAdapter.OnShopElementCLick listener;

    @AfterViews
    public void init() {

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        listener = new MainShopAdapter.OnShopElementCLick() {
            @Override
            public void onShopArticleClick(int id) {
                final MainActivity activity = (MainActivity) getActivity();
                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }
                activity.showShopCategory(getFragmentManager(), id);

            }
        };

        getData();
    }

    private void getData() {
        Call<List<Category>> call = ServiceGenerator.createService(Client.class).getCategory();

        final MainActivity activity = (MainActivity) getActivity();
        swipeRefreshLayout.setRefreshing(true);

        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }
                swipeRefreshLayout.setRefreshing(false);
                if (response == null) {
                    return;
                }
                List<Category> mainCategoryList = response.body();
                if (mainCategoryList == null || mainCategoryList.isEmpty()) {
                    return;
                }

                mainShopAdapter = new MainShopAdapter(activity, mainCategoryList, listener);

                shopList.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
                shopList.setAdapter(mainShopAdapter);
                mainShopAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                if (activity == null || activity.isFinishing() || !isAdded()) {
                    return;
                }
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(activity, "Internet greska", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
