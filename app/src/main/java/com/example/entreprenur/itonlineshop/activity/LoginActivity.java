package com.example.entreprenur.itonlineshop.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.entreprenur.itonlineshop.R;
import com.example.entreprenur.itonlineshop.fragment.LoginFragment;
import com.example.entreprenur.itonlineshop.fragment.LoginFragment_;
import com.example.entreprenur.itonlineshop.fragment.RegisterFragment_;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

import java.util.List;

@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FragmentManager fm = getSupportFragmentManager();
        if (fm != null) {
            fm.beginTransaction().add(R.id.login_container, LoginFragment_.builder().build(), LoginFragment_.class.getSimpleName())
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        }
    }


    @UiThread
    public void navigateToRegister(FragmentManager fragmentManager) {
        if (fragmentManager == null) {
            return;
        }
        fragmentManager.beginTransaction().replace(R.id.login_container, RegisterFragment_.builder().build(), RegisterFragment_.class.getSimpleName())
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null && fragmentManager.getFragments() != null && !fragmentManager.getFragments().isEmpty()) {
            if (fragmentManager.getFragments().get(0).getTag().equals("RegisterFragment_")) {
                super.onBackPressed();
            } else {
                finish();
            }
        }
    }

    public void navigateToMain() {
        MainActivity_.IntentBuilder_ builder = MainActivity_.intent(LoginActivity.this);
        builder.start();
        finish();
    }
}
